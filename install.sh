#!/bin/sh
# Quick script to install NixOS with a Home Assistant container running
#
# Ref: https://github.com/nix-community/disko/blob/master/docs/disko-install.md#fresh-installation
# sudo nix run 'github:nix-community/disko#disko-install' -- --flake <flake-url>#<flake-attr> --disk <disk-name> <disk-device>

# Ref: https://github.com/nix-community/disko/blob/master/disko-install
#   Usage: $0 [OPTIONS]
#     --mode MODE                 Specify the mode of operation. Valid modes are: format, mount.
#                                 Format will format the disk before installing.
#                                 Mount will mount the disk before installing.
#                                 Mount is useful for updating an existing system without losing data.
#     -f, --flake FLAKE_URI#ATTR  Use the specified flake to install the NixOS configuration.
#     --disk NAME DEVICE          Map the specified disk name to the specified device path.
#     --dry-run                   Print the commands that would be run, but do not run them.
#     --show-trace                Show the stack trace on error.
#     -h, --help                  Show this help message.
#     --extra-files SOURCE DEST   Copy the specified file or directory from the host into the NixOS configuration.
#     --option NAME VALUE         Pass the specified option to Nix.
#     --write-efi-boot-entries    Write EFI boot entries to the NVRAM of the system for the installed system.
#                                 Specify this option if you plan to boot from this disk on the current machine,
#                                 but not if you plan to move the disk to another machine.
#     --system-config JSON        Merges the specified JSON object into the NixOS configuration.

if [ $(id -u) -ne 0 ] ; then
  echo "Please run using sudo"
  exit 1
fi

nix --extra-experimental-features 'flakes nix-command' run 'github:nix-community/disko#disko-install' -- --flake 'gitlab:cryptochasm/nixos-home-assistant#home-assistant' --disk main /dev/vda


{ config, lib, pkgs, ... }:
let
  # Local customization:
  hostName = "home-assistant";
  timeZone = "America/Los_Angeles";
  authorizedKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC154sb98A0WlOSWOZK40h5UWOpaecOXeY34wI4GXLFp";
  # Home Assistant container tag:
  imageTag = "2024.6.1";
  # Get the latest tag from here, but use the explicit tag, not "stable" or "latest":
  # https://github.com/home-assistant/core/pkgs/container/home-assistant/226083991?tag=stable

in
{
  nix = {
    gc.automatic = true;
    settings.experimental-features = [ "nix-command" "flakes" ];
  };

  imports = [ ./hardware-configuration.nix ];

  networking.hostName = "${hostName}";
  time.timeZone = "${timeZone}";

  boot.loader.grub.enable = true;
  services.openssh.enable = true;

  users.users.root = {
    openssh.authorizedKeys.keys = [ "${authorizedKey}" ];
    packages = with pkgs; [ 
      git 
    ];
  };

  systemd.tmpfiles.rules = [
    "d /srv/home-assistant 0700 root root -"
  ];

  # Ref: https://nixos.wiki/wiki/Home_Assistant#OCI_container
  virtualisation = {
    oci-containers = {
      backend = "podman";
      containers.homeassistant = {
#        volumes = [ "home-assistant:/config" ];
        volumes = [ "/srv/home-assistant:/config" ];
        environment.TZ = "${timeZone}";
        image = "ghcr.io/home-assistant/home-assistant:${imageTag}"; # Warning: if the tag does not change, the image will not be updated
        extraOptions = [ 
          "--network=host" 
        ];
      };
    };
  };

  # Allow traffic to the Home Assistant container
  networking.firewall.allowedTCPPorts = [ 8123 ];

  system.stateVersion = "24.05"; # DO NOT CHANGE
}

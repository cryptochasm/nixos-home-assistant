{
  # Ref: https://github.com/nix-community/disko/blob/master/docs/HowTo.md#using-the-nixos-module
  # Ref: https://github.com/nix-community/disko/blob/master/example/hybrid.nix
  disko.devices = {
    disk = {
      main = {
        type = "disk";
        # Use '--disk main /dev/vda' or similar to specify device name at runtime
        device = "/dev/vda";
        content = {
          type = "gpt";
          partitions = {
            boot = {
              size = "1M";
              type = "EF02"; # for grub MBR
              priority = 1; # Needs to be first partition
            };
            ESP = {
              size = "512M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            root = {
              size = "100%";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/";
              };
            };
          };
        };
      };
    };
  };
}


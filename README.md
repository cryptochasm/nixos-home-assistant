# NixOS Home Assistant

This is a Nix Flake intended to help quickly initialize a virtual machine running Home Assistant in a OCI Container.

## Usage

_This is a work in progress.  Use at your own risk._

To use, clone this repo and change `authorizedKey` in `configuration.nix` to your own public SSH key.

Launch a NixOS virtual machine using the minimal ISO: https://nixos.org/download/#nixos-iso

Run the following command, and reboot when it completes:
```bash
curl -s https://gitlab.com/cryptochasm/nixos-home-assistant/-/raw/main/install.sh | sudo /bin/sh
```

Once the new machine comes up, ssh to it as root, and run the following commands:
```bash
git clone https://gitlab.com/cryptochasm/nixos-home-assistant.git
rmdir /etc/nixos
ln -s /root/nixos-home-assistant /etc/nixos
nixos-rebuild switch
```

That last command should not change anything, but will confirm that everything is working.

Home assistant will use the /srv/home-assistant directory to mount its volume.  Restore any backups to that location.

## References
 - https://nixos.wiki/wiki/Home_Assistant#OCI_container
 - https://github.com/nix-community/disko/blob/master/docs/HowTo.md
 - https://github.com/nix-community/disko/blob/master/docs/disko-install.md#fresh-installation
 - https://github.com/nix-community/disko/blob/master/disko-install

